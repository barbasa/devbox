#!/bin/bash

set -x
echo I am provisioning...
date > /etc/vagrant_provisioned_at

debconf-set-selections <<< 'mysql-server mysql-server/root_password password admin'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password admin'
apt-get update

echo 'INSTALLING MYSQL'
apt-get install -y mysql-server
mysql -u root --password=admin -e 'CREATE DATABASE IF NOT EXISTS rest_demo CHARACTER SET utf8 COLLATE utf8_bin;'
mysql -u root --password=admin -e "GRANT ALL PRIVILEGES ON rest_demo.* TO 'rest_demo'@'%' IDENTIFIED BY 'rest_demo' WITH GRANT OPTION;"
mysql -u root --password=admin -e 'FLUSH PRIVILEGES;'

mysql -u root --password=admin -e 'CREATE DATABASE IF NOT EXISTS test1 CHARACTER SET utf8 COLLATE utf8_bin;'
mysql -u root --password=admin -e "GRANT ALL PRIVILEGES ON test1.* TO 'rest_demo'@'%' IDENTIFIED BY 'rest_demo' WITH GRANT OPTION;"
mysql -u root --password=admin -e 'FLUSH PRIVILEGES;'

echo 'INSTALLING UTILS'
sudo apt-get install -y vim
sudo apt-get install -y curl
sudo apt-get install -y git

echo 'INSTALLING JAVA'
sudo apt-get install -y python-software-properties
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
sudo apt-get install -y oracle-java7-installer
java -version
if [ "$?" = "1" ]
then
   echo 'ERROR: Java could not be installed. Exiting...'
   exit 1
else
   echo 'Java installation was succesful'
fi

echo 'INSTALLING SOLR'
sudo apt-get install -y solr-tomcat
sudo grep ^JAVA_HOME /etc/default/tomcat6
if [ "$?" = "1" ]
then
   echo 'Setting JAVA_HOME for tomcat6...'
   sudo echo 'JAVA_HOME=/usr/lib/jvm/java-7-oracle' | sudo tee -a /etc/default/tomcat6
   sudo service tomcat6 start
fi

echo 'INSTALLING MAVEN'
sudo apt-get install -y maven

export MY_IP=`ifconfig eth1 | grep 'inet addr:' | cut -d':' -f2 | cut -d' ' -f1`
echo
echo "* * * Your IP ADDRESS is: $MY_IP"
echo
